package com.example.emanueleserrao.it.europebrowse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by emanueleserrao on 07/05/18.
 */

public class SQLhelperCountries extends SQLiteOpenHelper {

    private static final String DB_NAME = "europe.db";
    private static final int VERSION = 1;

    // columns & db parameters
    private static final String TABLE_NAME = "europe";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DISPLAY_NAME = "disp_name";
    private static final String COLUMN_CAPITAL = "capital";
    private static final String COLUMN_POP = "population";

    public SQLhelperCountries(Context context) {
        super(context, DB_NAME, null, VERSION);
        //context.deleteDatabase(DB_NAME);
    }

    private ArrayList<Country> listCountries(){

        ArrayList<Country> countries = new ArrayList<>();

        countries.add(new Country("albania","Albania"	,"Tirana","2,887,000"));
        countries.add(new Country("andorra","Andorra","Andorra la Vella", "78,000"));
        countries.add(new Country("armenia","Armenia"	,"Yerevan", "2,925,000"));
        countries.add(new Country("austria","Austria","Vienna", "8,751,820"));
        countries.add(new Country("azerbaijan","Azerbaijan","Baku", "9,923,914"));
        countries.add(new Country("belarus","Biellorussia","Minsk", "9,452,113"));
        countries.add(new Country("belgium","Belgio","Bruxelles", "11,469,204"));
        countries.add(new Country("bosnia","Bosnia and Herzegovina","Sarajevo", "3,750,000"));
        countries.add(new Country("bulgaria","Bulgaria","Sofia", "7,036,848"));
        countries.add(new Country("croatia","Croazia","Zagreb", "4,230,000"));
        countries.add(new Country("cyprus","Cipro","Nicosia", "876,000"));
        countries.add(new Country("czech","Repubblica ceca","Praga", "10,625,250"));
        countries.add(new Country("denmark","Danimarca","Copenhagen", "5,745,547"));
        countries.add(new Country("estonia","Estonia","Tallinn", "1,315,000"));
        countries.add(new Country("finland","Finlandia","Helsinki", "5,475,000"));
        countries.add(new Country("france","Francia","Parigi", "65,233,271"));
        countries.add(new Country("georgia","Georgia","Tbilisi", "3,707,000"));
        countries.add(new Country("germany","Germania","Berlino", "82,521,653"));
        countries.add(new Country("greece","Grecia","Atene", "11,142,161"));
        countries.add(new Country("hungary","Ungheria","Budapest", "9,702,671"));
        countries.add(new Country("iceland","Islanda","Reykjavik", "331,000"));
        countries.add(new Country("ireland","Irlanda","Dublino", "4,630,000"));
        countries.add(new Country("italy","Italia","Roma", "60,494,118"));
        countries.add(new Country("kosovo","Kosovo","Pristina", "1,867,000"));
        countries.add(new Country("latvia","Lettonia","Riga", "1,953,000"));
        countries.add(new Country("liechtenstein","Liechtenstein","Vaduz", "37,000"));
        countries.add(new Country("lithuania","Lituania","Vilnius", "2,803,000"));
        countries.add(new Country("luxembourg","Lussemburgo","Lussemburgo", "570,000"));
        countries.add(new Country("macedonia","Repubblica di Macedonia","Skopje", "2,071,000"));
        countries.add(new Country("malta","Malta","Valletta", "425,000"));
        countries.add(new Country("moldova","Moldavia","Chisinau", "3,564,000"));
        countries.add(new Country("monaco","Monaco","Monaco", "37,000"));
        countries.add(new Country("montenegro","Montenegro","Podgorica", "620,000"));
        countries.add(new Country("netherlands","Olanda","Amsterdam", "17,084,459"));
        countries.add(new Country("norway","Norvegia","Oslo", "5,194,000"));
        countries.add(new Country("poland","Polonia","Varsavia", "38,104,832"));
        countries.add(new Country("portugal","Portogallo","Lisbona", "10,291,196"));
        countries.add(new Country("romania","Romania","Bucharest", "19,622,000"));
        countries.add(new Country("russia","Russia","Moscow", "143,964,709"));
        countries.add(new Country("san","San Marino","San Marino", "33,000"));
        countries.add(new Country("serbia","Serbia","Belgrado", "8,762,027"));
        countries.add(new Country("slovakia","Slovacchia","Bratislava", "5,426,000"));
        countries.add(new Country("slovenia","Slovenia","Ljubljana", "2,065,000"));
        countries.add(new Country("spain","Spagna","Madrid", "46,397,452"));
        countries.add(new Country("sweden","Svezia","Stoccolma", "10,135,303"));
        countries.add(new Country("switzerland","Svizzera","Berna", "8,544,034"));
        countries.add(new Country("turkey","Turchia","Ankara", "80,810,000"));
        countries.add(new Country("ukraine","Ucraina","Kyiv (Kiev)", "42,895,704"));
        countries.add(new Country("united","Regno Unito","Londra", "65,110,276"));
        countries.add(new Country("vatican","Città del Vaticano","Città del Vaticano", "1,000"));


        return countries;
    }

    private void insertCountryRow(Country county, SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, county.getName());
        values.put(COLUMN_DISPLAY_NAME, county.getDisplayName());
        values.put(COLUMN_CAPITAL, county.getCapital());
        values.put(COLUMN_POP, county.getPopulation());

        db.insert(TABLE_NAME,null,  values);
    }

    private void initDB(SQLiteDatabase db){
        ArrayList<Country> mainList = this.listCountries();
        for(Country c: mainList){
            this.insertCountryRow(c, db);
        }
    }

    public ArrayList<Country> getCountriesByName(String name){

        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                new String[]{COLUMN_NAME, COLUMN_DISPLAY_NAME, COLUMN_CAPITAL, COLUMN_POP},
                COLUMN_DISPLAY_NAME + " LIKE ?", new String[]{"%" +  name + "%"},
                null, null, null, null);

        ArrayList<Country> returnedList = new ArrayList<>();

        if (cursor == null || cursor.getCount() == 0){
            return returnedList;
        }

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){

            // prepare note object
            returnedList.add(new Country(
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_DISPLAY_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_CAPITAL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_POP))));

            cursor.moveToNext();
        }


        // close the db connection
        cursor.close();

        db.close();

        return returnedList;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String query =
                "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME +" (" +
                "            _id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "            "+COLUMN_NAME+" STRING NULL," +
                "            "+COLUMN_DISPLAY_NAME+" STRING NULL," +
                "            "+COLUMN_CAPITAL+" STRING NULL," +
                "            "+COLUMN_POP+" String DEFAULT NULL" +
                "            )";
        db.execSQL(query);

        this.initDB(db);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
