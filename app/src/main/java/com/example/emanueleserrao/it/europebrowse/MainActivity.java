package com.example.emanueleserrao.it.europebrowse;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, TextWatcher{

    SQLhelperCountries dbHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.dbHelper = new SQLhelperCountries(this);

        EditText placesText = findViewById(R.id.Place_ET_place);
        placesText.addTextChangedListener(this);

        ListView listView = findViewById(R.id.Place_LV_places);
        listView.setOnItemClickListener(this);

    }

    public void findByName(String name){

        TextView badSearch = findViewById(R.id.Place_TV_badsearch);
        ListView listView = findViewById(R.id.Place_LV_places);

        if(name.length() < 2){
            listView.setVisibility(View.GONE);
            return;
        }

        ArrayList<Country> country_list = this.dbHelper.getCountriesByName(name);

        if(country_list.size() == 0){
            badSearch.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }else{
            badSearch.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);

            CountryAdapter adapter = new CountryAdapter(getApplicationContext(), country_list);
            listView.setAdapter(adapter);
        }
    }

    private void goToPdfActivity(String country_name){
        Intent intent = new Intent(this, PdfActivity.class);
        intent.putExtra("country_name", country_name);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ListView listView = findViewById(R.id.Place_LV_places);
        Country country = (Country) listView.getAdapter().getItem(position);
        this.goToPdfActivity(country.getName() + ".pdf");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        this.findByName(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}




