package com.example.emanueleserrao.it.europebrowse;

/**
 * Created by emanueleserrao on 07/05/18.
 */

public class Country {

    private String name;
    private String displayName;
    private String capital;
    private String population;

    public Country(String name, String displayName, String capital, String population) {
        this.name = name;
        this.displayName = displayName;
        this.capital = capital;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName(){
        return displayName;
    }

    public String getCapital() {
        return capital;
    }

    public String getPopulation() {
        return population;
    }
}
