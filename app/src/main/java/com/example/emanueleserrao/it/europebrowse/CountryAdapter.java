package com.example.emanueleserrao.it.europebrowse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by emanueleserrao on 07/05/18.
 */

public class CountryAdapter  extends BaseAdapter {

    private ArrayList<Country> data = null;
    private static LayoutInflater inflater = null;

    private Context context = null;

    public CountryAdapter(Context context, ArrayList<Country> data) {
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.context = context;
    }

    @Override
    public int getCount() {

        if(this.data == null){
            return 0;
        }

        return this.data.size();
    }

    @Override
    public Object getItem(int position) {
        if(this.data == null){
            return null;
        }

        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(this.data == null){
            return null;
        }

        View target_view = convertView;

        if (target_view == null){
            target_view = inflater.inflate(R.layout.list_row, null);
        }

        TextView text_light = target_view.findViewById(R.id.List_TV_row_light);
        TextView text_bold = target_view.findViewById(R.id.List_TV_row_bold);

        Country p = data.get(position);
        text_bold.setText(p.getDisplayName());
        text_light.setText(p.getCapital());

        addFlagImage(p.getName(), target_view);

        return target_view;
    }

    private void addFlagImage(String name, View target_view){

        FrameLayout flag_icon_layout = target_view.findViewById(R.id.List_FL_flag);
        int flag_icon_id = this.context.getResources().getIdentifier(name, "drawable", this.context.getPackageName());
        if(flag_icon_id != 0){
            flag_icon_layout.setBackgroundResource(flag_icon_id);
        }
    }
}
